import React from "react";
import PropTypes from "prop-types";
import "./Button.scss";

/**
 * Primary UI component for user interaction
 */

export const Button = ({
  outline,
  className,
  type,
  backgroundColor,
  size,
  label,
  ...props
}) => {
  const outlineType = outline ? "outline" : "";
  return (
    <button
      type="button"
      className={[className, "xor-button", size, outlineType, type].join(" ")}
      style={backgroundColor && { backgroundColor }}
      {...props}
    >

      {label}
    </button>
  );
};

Button.propTypes = {
  /**
   * Is this the action on the page?
   */
  type: PropTypes.oneOf(["primary", "secondary", "large"]),
  /**
   * add class to Button
   */
  className: PropTypes.string,
  /**
   * What background color to use
   */
  backgroundColor: PropTypes.string,
  /**
   * How large should the button be?
   */
  size: PropTypes.oneOf(["small", "default", "large"]),
  /**
   * Button contents
   */
  label: PropTypes.string.isRequired,
  /**
   * Outline type Button
   */
  outline: PropTypes.bool,
  /**
   * Optional click handler
   */
  onClick: PropTypes.func,
};

Button.defaultProps = {
  backgroundColor: null,
  type: "primary",
  label: "Button",
  size: "default",
  onClick: undefined,
  outline: false,
};


// 30181160 id