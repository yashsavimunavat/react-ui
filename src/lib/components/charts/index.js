import React, { forwardRef } from 'react';
import ChartComponent from './Charts';

export const Line = forwardRef((props, ref) => (
	<ChartComponent
		{...props}
		type='line'
		ref={ref}
		options={props.options || {}}
	/>
));

export const Bar = forwardRef((props, ref) => (
	<ChartComponent
		{...props}
		type='bar'
		ref={ref}
		options={props.options || {}}
	/>
));

export const Pie = forwardRef((props, ref) => (
	<ChartComponent
		{...props}
		type='pie'
		ref={ref}
		options={props.options || {}}
	/>
));

export const Doughnut = forwardRef((props, ref) => (
	<ChartComponent
		{...props}
		type='doughnut'
		ref={ref}
		options={props.options || {}}
	/>
));
