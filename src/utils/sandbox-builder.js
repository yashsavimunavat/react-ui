import React, { useCallback, useEffect, useState } from "react";
import { getParameters } from "./parameter-builder";

export const Sandbox = ({ platform, pathToOpen }) => {
  const [sandboxId, setSandboxId] = useState("");
  const [error, setError] = useState(null);

  const fetchSandboxId = useCallback(async () => {
    try {
      const params = await getParameters(platform);
      const response = await fetch(
        "https://codesandbox.io/api/v1/sandboxes/define?json=1",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
          body: JSON.stringify(params),
        }
      );
      const data = await response.json();
      setSandboxId(data.sandbox_id);
    } catch (e) {
      setError("Something went wrong!");
    }
  }, [platform]);

  useEffect(() => {
    fetchSandboxId();
  }, [fetchSandboxId]);

  return (
    <>
      {!!sandboxId && !error && (
        <>
          <iframe
            src={`https://codesandbox.io/embed/${sandboxId}?fontsize=14&view=editor&hidenavigation=1&module=${pathToOpen}&theme=dark`}
            title={`${platform}-component`}
            style={{
              margin: "5px",
              flex: 1,
              height: "500px",
              border: "1px solid black",
              borderRadius: "4px",
              overflow: "hidden",
            }}
            allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr; xr-spatial-tracking"
            sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
          ></iframe>
        </>
      )}
      {error && <p>{error}</p>}
    </>
  );
};
