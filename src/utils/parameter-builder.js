import { Gitlab } from "@gitbeaker/browser";
import { ProjectsBundle } from "gitlab";

const ref = "master";

const api = new ProjectsBundle({
  token: process.env.REACT_APP_GITLAB_TOKEN,
});

/**
 * Fetch raw file from the Gitlab Repository using Gitlab api.
 * @param {string} projectId - Project ID of the Repository.
 * @param {string} path - Path of the specified file.
 * @returns {Promise} - Raw file in string format
 */
const getRawFile = async (projectId, path) => {
  return api.RepositoryFiles.showRaw(projectId, path, ref);
};

/**
 * Fetch Repository tree that contains all the files with their details.
 * @param {string} projectId - Project ID of the Repository.
 * @returns {Promise} - Array of objects where each object
 *                      corresponds to individual file.
 */
const getRepoTree = async (projectId) => {
  return api.Repositories.tree(projectId, { recursive: true });
};

/**
 * Retrieve only the files that are required.
 * @param {string} projectId - Project ID of the Repository.
 * @returns {Promise} - Array of strings of required filenames.
 */
const getFiles = async (projectId) => {
  const filesObj = await getRepoTree(projectId);
  const filesArray = [...filesObj];
  const filenames = filesArray
    .filter((file) => (file.type === "tree" ? false : true))
    .map((file) => file.path);
  const excludeFiles = [
    "package-lock.json",
    "public/logo192.png",
    "public/logo512.png",
    "public/favicon.ico",
    "src/favicon.ico",
  ];
  const requiredFiles = filenames.filter(
    (file) => !excludeFiles.includes(file)
  );
  return requiredFiles;
};

/**
 * Builds the object that is required by codesandbox api.
 * @param {string} platform - React or Angular.
 * @returns {Promise} - Object containing all the files
 *                      and their raw content.
 * @see {@link https://codesandbox.io/docs/api/#define-api}
 */
export const getParameters = async (platform) => {
 // abc();
  const projectId =
    platform === "react"
      ? process.env.REACT_APP_REACT_ID
      : process.env.REACT_APP_ANGULAR_ID;
  const paramFiles = await getFiles(projectId);
  let parameters = { files: {} };

  await Promise.all(
    paramFiles.map(async (filename) => {
      return (parameters.files[filename] = {
        content: await getRawFile(projectId, filename),
      });
    })
  );
  
  return parameters;
};
