import React from "react";
import { Bar, Doughnut, Line, Pie } from "../lib/components/charts";

export default {
  title: "Example/Chart",
  component: Line,
  argTypes: {
    backgroundColor: { control: "color" },
  },
};
const SCALE = {
  width: "700",
  height: "500",
};
const LineData = {
  labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
  datasets: [
    {
      label: "# of Votes",
      data: [12, 19, 3, 5, 2, 3],
      fill: true,
    },
  ],
};

const LineOptions = {
  responsive: false,
};


const BarData = {
  labels: [1,2,3,4,5,6,7],
  datasets: [{
    label: 'My First Dataset',
    data: [65, 59, 80, 81, 56, 55, 40],
    backgroundColor: [
      'rgba(255, 99, 132, 0.2)',
      'rgba(255, 159, 64, 0.2)',
      'rgba(255, 205, 86, 0.2)',
      'rgba(75, 192, 192, 0.2)',
      'rgba(54, 162, 235, 0.2)',
      'rgba(153, 102, 255, 0.2)',
      'rgba(201, 203, 207, 0.2)'
    ],
    borderColor: [
      'rgb(255, 99, 132)',
      'rgb(255, 159, 64)',
      'rgb(255, 205, 86)',
      'rgb(75, 192, 192)',
      'rgb(54, 162, 235)',
      'rgb(153, 102, 255)',
      'rgb(201, 203, 207)'
    ],
    borderWidth: 1
  }]
};
const PieData = {
  labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
  datasets: [
    {
      label: "# of Votes",
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: [
        "rgba(255, 99, 132, 0.2)",
        "rgba(54, 162, 235, 0.2)",
        "rgba(255, 206, 86, 0.2)",
        "rgba(75, 192, 192, 0.2)",
        "rgba(153, 102, 255, 0.2)",
        "rgba(255, 159, 64, 0.2)",
      ],
      borderColor: [
        "rgba(255, 99, 132, 1)",
        "rgba(54, 162, 235, 1)",
        "rgba(255, 206, 86, 1)",
        "rgba(75, 192, 192, 1)",
        "rgba(153, 102, 255, 1)",
        "rgba(255, 159, 64, 1)",
      ],
      borderWidth: 2,
    },
  ],
};
const LineTemplate = ({ ...args }) => (
  <div className="chart-container">
    <Line {...args} />
  </div>
);
const PieTemplate = ({ ...args }) => <Pie {...args} />;
const DoughnutTemplate = ({ ...args }) => <Doughnut {...args} />;
const BarTemplate=({...args}) =><Bar {...args}/>

export const LineChart = LineTemplate.bind({});
export const PieChart = PieTemplate.bind({});
export const DoughnutChart = DoughnutTemplate.bind({});
// export const BarChart=BarTemplate.bind({});

LineChart.args = {
  onclick: { action: "onclick" },
  id: "LINE_CHART",
  data: LineData,
  ...SCALE,
  options: {
    ...LineOptions,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Line Chart",
      },
    },
  },
};

PieChart.args = {
  id: "PIE_CHART 1",
  ...SCALE,
  data: PieData,
  options: {
    responsive: false,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Pie Chart",
      },
    },
  },
};

DoughnutChart.args = {
  onclick: { action: "onclick" },
  id: "DOUGHNUT_CHART",
  ...SCALE,
  data: PieData,
  options: {
    responsive: false,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Doughnut Chart",
      },
    },
  },
};


// BarChart.args = {
//   onclick: { action: "onclick" },
//   id: "BAR_CHART",
//   ...SCALE,
//   data: BarData,
//   options: {
//     responsive: false,
//     plugins: {
//       legend: {
//         position: "top",
//       },
//       title: {
//         display: true,
//         text: "BAR Chart",
//       },
//     },
//   },
// };
