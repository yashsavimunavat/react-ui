import React from "react";
import { Button } from "../lib/components/button";
import { Sandbox } from "../utils/sandbox-builder";
export default {
  title: "Example/Button",
  component: Button,
  argTypes: {
    backgroundColor: { control: "color" },
  },
};

const Template = ({ ...args }) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  onclick: { action: "onclick" },
  type: "primary",
  label: "Button",
};

export const Secondary = Template.bind({});
Secondary.args = {
  type: "secondary",
  label: "Button",
};

export const Danger = Template.bind({});
Danger.args = {
  type: "danger",
  label: "Button",
};

export const Outline = () => (
  <>
    <Button outline type="primary" label="Primary outline" />
    <Button outline type="secondary" label="Secondary outline" />
    <Button outline type="danger" label="Danger outline" />
  </>
);

export const Disabled = Template.bind({});
Disabled.args = {
  disabled: true,
  type: "primary",
  label: "Button",
};

export const Large = Template.bind({});
Large.args = {
  type: "primary",
  size: "large",
  label: "Button",
};

export const Small = Template.bind({});
Small.args = {
  type: "primary",
  size: "small",
  label: "Button",
};

export const CodeSandbox = () => (
  <div style={{ display: "flex", flexWrap: "wrap" }}>
    <Sandbox platform="react" pathToOpen="src%2FApp.js" />
    <Sandbox platform="angular" pathToOpen="%2Fsrc%2Fapp%2Fapp.component.ts" />
    
  </div>
);
